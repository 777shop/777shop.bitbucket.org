window.log=function(){log.history=log.history||[];log.history.push(arguments);if(this.console){arguments.callee=arguments.callee.caller;var a=[].slice.call(arguments);(typeof console.log==="object"?log.apply.call(console.log,console,a):console.log.apply(console,a))}};
(function(b){function c(){}for(var d="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,timeStamp,profile,profileEnd,time,timeEnd,trace,warn".split(","),a;a=d.pop();){b[a]=b[a]||c}})((function(){try
{console.log();return window.console;}catch(err){return window.console={};}})());

/*! http://mths.be/placeholder v2.0.7 by @mathias */
;(function(f,h,$){var a='placeholder' in h.createElement('input'),d='placeholder' in h.createElement('textarea'),i=$.fn,c=$.valHooks,k,j;if(a&&d){j=i.placeholder=function(){return this};j.input=j.textarea=true}else{j=i.placeholder=function(){var l=this;l.filter((a?'textarea':':input')+'[placeholder]').not('.placeholder').bind({'focus.placeholder':b,'blur.placeholder':e}).data('placeholder-enabled',true).trigger('blur.placeholder');return l};j.input=a;j.textarea=d;k={get:function(m){var l=$(m);return l.data('placeholder-enabled')&&l.hasClass('placeholder')?'':m.value},set:function(m,n){var l=$(m);if(!l.data('placeholder-enabled')){return m.value=n}if(n==''){m.value=n;if(m!=h.activeElement){e.call(m)}}else{if(l.hasClass('placeholder')){b.call(m,true,n)||(m.value=n)}else{m.value=n}}return l}};a||(c.input=k);d||(c.textarea=k);$(function(){$(h).delegate('form','submit.placeholder',function(){var l=$('.placeholder',this).each(b);setTimeout(function(){l.each(e)},10)})});$(f).bind('beforeunload.placeholder',function(){$('.placeholder').each(function(){this.value=''})})}function g(m){var l={},n=/^jQuery\d+$/;$.each(m.attributes,function(p,o){if(o.specified&&!n.test(o.name)){l[o.name]=o.value}});return l}function b(m,n){var l=this,o=$(l);if(l.value==o.attr('placeholder')&&o.hasClass('placeholder')){if(o.data('placeholder-password')){o=o.hide().next().show().attr('id',o.removeAttr('id').data('placeholder-id'));if(m===true){return o[0].value=n}o.focus()}else{l.value='';o.removeClass('placeholder');l==h.activeElement&&l.select()}}}function e(){var q,l=this,p=$(l),m=p,o=this.id;if(l.value==''){if(l.type=='password'){if(!p.data('placeholder-textinput')){try{q=p.clone().attr({type:'text'})}catch(n){q=$('<input>').attr($.extend(g(this),{type:'text'}))}q.removeAttr('name').data({'placeholder-password':true,'placeholder-id':o}).bind('focus.placeholder',b);p.data({'placeholder-textinput':q,'placeholder-id':o}).before(q)}p=p.removeAttr('id').hide().prev().attr('id',o).show()}p.addClass('placeholder');p[0].value=p.attr('placeholder')}else{p.removeClass('placeholder')}}}(this,document,jQuery));

// place any jQuery/helper plugins in here, instead of separate, slower script files.
var linkHelper = { getBase: function (e, t, n) { return "http://www.buy2016.com/r-" + e + "/" + t.toLowerCase().replace(/[^a-z0-9]+/g, "-") + "?sid=" + escape(n) }, appndLocId: function () { return "&location=" + escape(document.location.href) + (document.referrer == "" ? "" : "&referrer=" + escape(document.referrer)) }, getSrchLnk: function (e) { return this.getBase("0", e, "search") + this.appndLocId() }, setLink: function (e) { var t = e.getAttribute("data-cc"); var n = this.getBase(e.getAttribute("data-id"), e.getAttribute("data-name"), e.getAttribute("data-sid")); if (t != "us") n += "&countryCode=" + t; var r = this.appndLocId(); if ((n + r).length < 400) { n += r } e.href = n; return true } }

// Gumby is ready to go
Gumby.ready(function () {
    console.log('Gumby is ready to go...', Gumby.debug());

    // placeholder polyfil
    if (Gumby.isOldie || Gumby.$dom.find('html').hasClass('ie9')) {
        $('input, textarea').placeholder();
    }
});

// Oldie document loaded
Gumby.oldie(function () {
    console.log("This is an oldie browser...");
});

// Touch devices loaded
Gumby.touch(function () {
    console.log("This is a touch enabled device...");
});
var openModal = function () {
    $('#openModal').click();
};

var trackId663 = "218663-278-0";
var prosperentHelper = {
    affUrl: "http://prosperent.com/store/product/" + trackId663 + "/",
    affUrlUK: "http://prosperent.com/store/product/uk/" + trackId663 + "/",
    affUrlCA: "http://prosperent.com/store/product/ca/" + trackId663 + "/",
    getAffUrl: function (q) {
        return this.affUrl + "?sid=search&k=" + escape(q);
    }
};

var goToStore = function (countryCode, id, name, sid) {
    openModal();

    var url = "";
    if (countryCode == "US") {
        url = prosperentHelper.affUrl;
    }
    else if (countryCode == "CA") {
        url = prosperentHelper.affUrlCA;
    }
    else if (countryCode == "UK") {
        url = prosperentHelper.affUrlUK;
    }
    url += "?sid=" + escape(sid) + "&k=" + escape(name) + "&p=" + id + "&location=" + escape(document.location.href) +
        (document.referrer == "" ? "" : ("&referrer=" + escape(document.referrer)));

    document.location.href = url;
    return false;
};

var search = function () {
    var val = $("input#searchInput").val();
    if (val != "") {
        document.location.href = prosperentHelper.getAffUrl(val);
    }
};

// Document ready
$(function () {

});


